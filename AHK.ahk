; https://www.autohotkey.com/docs/KeyList.htm
; ^ = Control    + = Shift    ! = Alt

; ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
; ┃ LCtr+LAlt+LShf+d  Acceso Buzón Dixestivo ┃
; ┃ LCtr+LAlt+LShf+n  Acceso Buzón Neuro     ┃
; ┃ L-Ctr+n       "nrlx"                     ┃
; ┃ L-Ctr+f       "[feit"                    ┃
; ┃ L-Win         Copia NHC e Alt+Tab        ┃
; ┃ R-Shf         Acceso a VerHist           ┃
; ┃ R-Ctr         Sair paciente + Alt+Tab    ┃
; ┃ L-Ctr+0       Acceso Simple              ┃
; ┃ L-Ctr+9       Acceso Cita                ┃
; ┃ LCtr+. (TN)   Cambio de Situación        ┃
; ┃ + (TN)        Saltar Avisos cita         ┃
; ┃ F4            Pechar completamente       ┃
; ┃ `             "["                        ┃
; ┃ +             "]"                        ┃
; ┃ Inicio        F1                         ┃
; ┃ Fin           F11                        ┃
; ┃ LCtr+LShf+S   Save & Reload Script       ┃
; ┃ LCtr+m        Modificar cita de buzon    ┃
; ┃ LCtr+r        Revisión de data           ┃
; ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

; ┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅ Scripts temporais ┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅

; Temporal 1 - Revisar Buzon se hai cita dispoñible
;PgDn::
;Send {Enter}{Down}{F1}{F1}
;return

;PgDn::
;Send c{Down 3}covi10{down}{Enter}{F1}{enter}
;Send 1{enter 2}adm{down}{del 8}{down}otr{up}0{down}
;return

PgUp::
Send {f1 4}
return


; ┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅┅

;━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ BUZÓNS ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ LCtr+LAlt+LShf+d    Acceso Buzón Dixestivo ╍╍╍╍

<^<+<!d::
Send {Down 3}161021
Send {Down}071121
Send {Down 6}digc
Send {Down 3}2.2|2.3
Send {Down 6}pr
Send {Down}{F1}
return

;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ LCtr+LAlt+LShf+n    Acceso Buzón Neuro ╍╍╍╍╍╍╍╍

<^<+<!n::		; Acceso para buscar pacientes dispoñibles

;Send {Down 11}
;Send NRLxf1		; Modificar
;Send {Down 2}
;Send 8.32		; Modificar
;Send {Down 6}
;Send pr{Down}{F1}

Send {Down 3}		;Data idonea inicial
Send {Down}311221	;Data Idonea Final
Send {Down 6}nrlc	;Servizo
Send {Down}		;Axenda
Send {Down 2}2.1905	;Prestacion
Send {Down 6}pr
;Send {Up 6}		;Solo necesario para escribir prestacion manual, descomentar todo seguinte
Send {Up 8}NRLX

;Send {Down}{F1}            ;Descomentar para acceso automatico

return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ L-Ctr+m    Modificar cita de buzon ╍╍╍╍╍╍╍╍╍╍╍╍

<^m::
Send m{Down 7}nrlx	; Modificar
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ LCtr+. (TN)    Cambio de Situación ╍╍╍╍╍╍╍╍╍╍╍╍

<^NumpadDot::
Send s{Enter}{Down}pte rm{Down}241221{up}{right 4}	; Modificar
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ LCtr+r    Revisión de data ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍
<^r::
Send rs{Right 3}
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ L-Ctr+n    "nrlx" ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

<^n::
Send NRLX	; Modificar
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ L-Ctr+f    "[feit" ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

<^f::
Send [feit
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ L-Win    Copia NHC e Alt+Tab ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

LWin::
Send {LButton}
Sleep 50
Send {LButton}
Sleep 50
Send {Alt Down}{tab}{Alt up}
Sleep 50
Send {Ctrl Down}{Shift Down}v{Ctrl up}{Shift up}{Enter 2}
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ R-Shf    Acceso a VerHist ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

RShift::
Send {Enter}v{Enter}{F1}
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ R-Ctr    Sair paciente + Alt+Tab ╍╍╍╍╍╍╍╍╍╍╍╍╍╍

RControl::
Send {F11 1}	; Cambiable
Sleep 40
Send {F11}{Alt Down}{tab}{Alt up}
return


;━━━━━━━━━━━━━━━━━━━━━━━━━━━ FIN BUZÓNS ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━



;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ + (TN)    Saltar Avisos cita ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

NumpadAdd::
Send {Enter}ssssss
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ F4    Pechar completamente ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

F4::
Send {F11 9}s
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍  Fin    F11  ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

End::
Send {F11}
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍  Inicio  ===  F1  ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

Home::
Send {F1}
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍  `  ===  "["  ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

`::[
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍  +  ===  "]"  ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

+::]
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ L-Ctr+0    Acceso Simple ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

<^0::
Send dquivaz{Enter}
Sleep 10
Send 987654{Enter}
Sleep 1000
Send {Enter}
return


;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ L-Ctr+9    Acceso Cita ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍

<^9::
Send dquivaz{Enter}
Sleep 10
Send 987654{Enter}
Sleep 2000
Send {Enter}{Down}{Enter 3}{F11}
return

;╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍ LCtr+LShf+S    Save & Reload Script ╍╍╍╍╍╍╍╍╍╍╍

<^<+S::
Send {Ctrl Down}s{Ctrl Up}
Sleep 200
Reload
return
